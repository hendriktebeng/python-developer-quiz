import requests
import openpyxl
import json

# request base information
url = "https://tools.usps.com/tools/app/ziplookup/zipByAddress"
headers = {"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36"}
session = requests.Session()

# adress_check() - takes a list containing the adress information and sends the request; returns status
def adress_check(data):
     form_data = {
          "companyName": data[0],
          "address1": data[1],
          "address2": "",
          "city": data[2],
          "state": data[3],
          "urbanCode": "",
          "zip": data[4]
     }

     # post request
     request = session.post(
          url = url,
          data = form_data,
          headers = headers
     )

     # response parsed into dictionary
     response = json.loads(request.content)
     # result status says whether the adress is valid/invalid
     status = response["resultStatus"]
     return status

# accessing xlsx file
wb = openpyxl.load_workbook("adresses.xlsx")
sheet = wb.active

rows = sheet.max_row + 1
columns = sheet.max_column + 1

header = sheet.cell(row = 1, column = columns)
header.value = "Validity"

for i in range(2, rows):
     data = []

     for j in range(1, columns):
          cell = sheet.cell(row = i, column = j)
          data.append(cell.value)

     try:
          validity = adress_check(data)
     except:
          # if an error occurs, the adress will be qualified of invalid
          validity = "ADRESS NOT FOUND"

     last_cell = sheet.cell(row = i, column = columns)
     # adress validity inserted in the last cell or the row
     last_cell.value = validity

# saving xlsx file
wb.save("adresses.xlsx")